#!/bin/sh

# save_clone.sh --
#
#   This file allows the automatic reconditionning of a computer
#   by automating save clone with Clonezilla
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was tested and validated on Clonezilla live 3.0.1-8 i686/amd64
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

################################################################################
# Options to customize clone
################################################################################

NAME_CLONE=img_clone
#DEBUG="true"
#BEEP_OFF="true"

# Volume sonore de Clonezilla en % au niveau de l'utilitaire amixer aka alsamixer
VOLUME=80

# Remove # on lines below to choose the checksum options :
ENABLE_CHECKSUM_SHA1="-gs" ; # Generate image SHA1 checksums
#ENABLE_CHECKSUM_MD5="-gm" ; # Generate image MD5 checksums
#ENABLE_CHECKSUM_MD5_ALL_FILES_OS="-gmf" # Generate checksums for files in device after saving

OPTIONS_FOR_GENERATE_CHECKSUM="${ENABLE_CHECKSUM_SHA1} ${ENABLE_CHECKSUM_MD5} ${ENABLE_CHECKSUM_MD5_ALL_FILES_OS}"

# Remove only # on lines below to choose the compression option you need :
OPTION_FOR_COMPRESSION="-z9p"  ; # parallel zstd compression take small image like Gzip
#OPTION_FOR_COMPRESSION="-z9"  ; # zstd compression take small image like Gzip
#OPTION_FOR_COMPRESSION="-z8"  ; # lz4 compression
#OPTION_FOR_COMPRESSION="-z7"  ; # lzzip compression
#OPTION_FOR_COMPRESSION="-z6"  ; # lzip compression
#OPTION_FOR_COMPRESSION="-z5"  ; # xz compression
#OPTION_FOR_COMPRESSION="-z4"  ; # lzma compression
#OPTION_FOR_COMPRESSION="-z3"  ; # lzo compression
#OPTION_FOR_COMPRESSION="-z2"  ; # bzip2 compression
#OPTION_FOR_COMPRESSION="-z1"  ; # Gzip compression take smaller image
#OPTION_FOR_COMPRESSION="-z1p" ; # parallel Gzip compression take smaller image
#OPTION_FOR_COMPRESSION="-z0"  ; # No compression

# Remove only # on lines below to choose "check and repair source file system" option you need :
OPTION_FOR_CKECK_REPAIR="-fsck" ; # Interactively check and repair source file system before saving
#OPTION_FOR_CKECK_REPAIR="-sfsck" ; # Skip checking/repairing source file system

# Remove only # on lines below to choose the "check the saved image" option you need :
OPTION_FOR_CKECK_SAVE_IMAGE="" ; # Yes, check the saved image
#OPTION_FOR_CKECK_SAVE_IMAGE="-scs" ; # No, skip checking the saved image

# Remove only # on lines below to choose the encrypt option you need :
OPTION_FOR_ENCRYPT="-senc" ; # Not to encrypt the image
#OPTION_FOR_ENCRYPT="-enc" ; # Encrypt the image

# Remove only # on lines below to choose the finish option you need :
# The action to perform when everything is finished :
OPTION_FOR_FINISH="-p choose" ; # Choose reboot/shutdown/etc when everything is finished
#OPTION_FOR_FINISH="-p cmd" ; # Enter command line prompt
#OPTION_FOR_FINISH="-p reboot" ; # Reboot
#OPTION_FOR_FINISH="-p poweroff" ; # Shutdown

################################################################################

MAX_SIZE_ROOT_PREFERED="80 G"
MAX_SIZE_ROOT_PREFERED_KB="80000000"

################################################################################

# Debug
if [ ! -z ${DEBUG} ] ; then
    OPTIONS_FOR_GENERATE_CHECKSUM=""
    OPTION_FOR_CKECK_SAVE_IMAGE="-scs"
    MAX_SIZE_ROOT_PREFERED="20 G"
    MAX_SIZE_ROOT_PREFERED_KB="20000000"
fi

if [ -z ${DEBUG} ] ; then
    # Efface l'affichage précédant
    clear
fi

################################################################################

KEY_DRIVE=$(sudo blkid | grep LABEL=\"IMAGES\" | cut  -d ':' -f1)
DATE=`date -u +"%Y-%m-%d-%H-%M"`
LANGUE=$(echo ${LANG} | cut -d _ -f1)

YELLOW='\033[1;33m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
RED='\033[0;31m'
NC='\033[0m' # No Color


if [ ${LANGUE} = fr ]
then

    message_info_emmabuntus="Clé USB de réemploi par Emmabuntüs (https://emmabuntus.org)"
    message_info_sources="Les sources sont disponibles sur http://usb-reemploi.emmabuntus.org"
    message_identification_usb_ko="Attention : La clé USB avec l'étiquette \"IMAGES\" n'a pas été indentifiée. Taper sur une touche quelconque pour sortir du script"
    message_trouve_usb_avant="Clé USB trouvée sur"
    message_trouve_usb_apres=", mais ce n'est pas son emplacement habituel, ce qui signifie que vous avez plusieurs disques durs sur ce système."
    message_selection_usb="Veuillez entrer le numéro correspondant au disque cible de l'installation :"
    message_mauvais_nombre_disque_dur="Attention : Vous n'avez pas entré un nombre pour selectionner le disque. Taper sur une touche quelconque pour sortir du script"
    message_mauvais_selection_disque_dur="Attention : Vous n'avez pas entré un nombre correspondant à un disque. Taper sur une touche quelconque pour sortir du script"
    message_usb_disque_dur_avant="Clé USB trouvée sur"
    message_usb_disque_dur_apres="et disque sélectionné sur "
    message_schema_partition_url="voir : http://partclone.emmabuntus.org/"
    message_schema_partition_ko1="Appuyez sur la touche \"Entrée\" pour continuer,\nmême si le clone ne sera pas compatible avec la clé de réemploi,"
    message_schema_partition_ko2="sinon tapez sur la touche [E] puis \"Entrée\" pour quitter le script ?"
    message_taille_usb_trop_petite_debut="La taille de la clé USB"
    message_taille_usb_trop_petite_milieu="est trop faible"
    message_taille_usb_trop_petite_fin="pour contenir"
    message_taille_usb_trop_petite_ko="Le script va s'interrompre, et appuyez sur la touche \"Entrée\" pour sortir du script ?"
    message_taille_partition_non_trouve1="Le script n'a pas pu déterminer la taille utilisée par la partition \"/\" ou celle de la clé USB,\nappuyez sur la touche \"Entrée\" pour continuer de toute façon,"
    message_taille_partition_non_trouve2=${message_schema_partition_ko2}
    message_taille_partition_superieure_avant="La taille de la partition"
    message_taille_partition_superieure_apres="est supérieure à"
    message_nb_partition_linux="Le disque dur contient plus d'une partition système, "
    message_nb_partition_linux_non_compatible="ce qui rend ce clone incompatible avec notre schéma de partition, "
    message_taille_partition_root="ce qui rend ce clone moins générique, car vous ne pourrez le cloner\nque sur des disques durs ayant au minimun cette taille"
    message_taille_partition_root_conseil="Si vous avez un disque dur de grande taille nous vous conseillons de ne partitionner\nqu'une partie de celui-ci et de laisser le reste vide"
    message_schema_partition_grande_ko1="Appuyez sur la touche \"Entrée\" pour continuer,\nmême si le clone ne sera pas générique car la partition est trop grande,"
    message_schema_partition_grande_ko2="${message_schema_partition_ko2}"
    message_nombre="Saisissez un nombre ci-dessus, puis validez : "
    symbole_octet="o"
    controle_partition="Contrôle de l'intégrité et réparation de la partition : "
    message_taille="taille"
    message_taille_utilisee="utilisée"

else

    message_info_emmabuntus="Emmabuntüs reusable USB key (https://emmabuntus.org)"
    message_info_sources="Sources are available on http://usb-reemploi.emmabuntus.org"
    message_identification_usb_ko="Warning : The USB key with the \"IMAGES\" label was not found. Press any key to exit the script"
    message_trouve_usb_avant="USB Key found on"
    message_trouve_usb_apres=", but this is not it's usual location, which means that there are several hard drives on your computer."
    message_selection_usb="Please enter the number corresponding to the target disk of this install."
    message_mauvais_nombre_disque_dur="Warning : You did not enter a number to select the disk. Press any key to exit the script"
    message_mauvais_selection_disque_dur="Warning : You did not enter a number corresponding to a disk. Press any key to exit the script"
    message_usb_disque_dur_avant="USB Key found on"
    message_usb_disque_dur_apres="and selected hard drive on "
    message_schema_partition_apres="not compatible with the reuse key,"
    message_schema_partition_url="see : http://partclone.emmabuntus.org/"
    message_schema_partition_ko1="Press the \"Enter\" key to continue,\neven if the clone won't be compatible with the reuse key, "
    message_schema_partition_ko2="otherwise hit the [E] key then \"Enter\" to quit the script?"
    message_taille_usb_trop_petite_debut="The size of the USB key"
    message_taille_usb_trop_petite_milieu="is too small"
    message_taille_usb_trop_petite_fin="to contain"
    message_taille_usb_trop_petite_ko="The script will abort, and press the \"Enter\" key to exit the script?"
    message_taille_partition_non_trouve1="The script could not find out the size of the \"/\" partition or that of the USB key,\npress the \"Enter\" key to continue anyway,"
    message_taille_partition_non_trouve2=${message_schema_partition_ko2}
    message_taille_partition_superieure_avant="Partition size "
    message_taille_partition_superieure_apres="is greater than "
    message_nb_partition_linux="The hard drive contains more than one system partition, "
    message_nb_partition_linux_non_compatible="which makes this clone incompatible with our partition scheme, "
    message_taille_partition_root="which makes this clone less generic, because you can only clone it on hard drives with at least this size "
    message_taille_partition_root_conseil="If you have a large hard disk, we advise you to partition\nonly part of it and leave the rest empty"
    message_schema_partition_grande_ko1="Press the \"Enter\" key to continue,\neven if the clone won't be generic because the partition is too large, "
    message_schema_partition_grande_ko2="${message_schema_partition_ko2}"
    message_nombre="Enter a number above, then validate: "
    symbole_octet="B"
    controle_partition="Partition integrity check and repair: "
    message_taille="size"
    message_taille_utilisee="used"
fi

CODE_EXIT_MINUSCULE="e"
CODE_EXIT_MAJUSCULE="E"

echo ""
echo ${message_info_emmabuntus}
echo ${message_info_sources}
echo ""

# Verrouillage du pavé numérique
setleds -D +num


# Active un son à la fin des opérations de Clonezilla
if [ -z ${BEEP_OFF} ] ; then

    # Activation du son de Clonzilla
    CLONEZILLA_BEEP="-ps"

    # Modification du volume de la carte son
    amixer -q -c 0 set Master ${VOLUME}%
    amixer -q -c 0 set PCM ${VOLUME}%

else
    CLONEZILLA_BEEP=""
fi


# Identification de la clé USB
# Finding out the USB Key
if [ -z ${KEY_DRIVE} ] ; then

   echo -n "${RED}${message_identification_usb_ko}${NC}"
   echo "\a"

   read read_no_correct_label_key

   exit 0

elif [ $(echo ${KEY_DRIVE} | grep sdb) ] ; then

    KEY_DRIVE_FOUND="/dev/sdb"

elif [ $(echo ${KEY_DRIVE} | grep sda) ] ; then

    KEY_DRIVE_FOUND="/dev/sda"

fi

# Identification du disque dur
# Finding the hard drive

find_disk=$(sudo fdisk -l | grep 'Disk /dev/sd\|Disk /dev/hd\|Disk /dev/mmcblk\|Disk /dev/hd\|Disk /dev/nvme' | cut -d : -f1 | cut -d " " -f2)
nb_find_disk=$(echo ${find_disk} | wc -w)

if [ ! -z ${DEBUG} ] ; then
    echo "KEY_DRIVE_FOUND=${KEY_DRIVE_FOUND}"
    echo "find_disk=${find_disk}"
    echo "nb_find_disk=${nb_find_disk}"
fi

if [ ${nb_find_disk} -eq 2 ] && [ -n ${KEY_DRIVE_FOUND}  ] ; then # Si 2 disques trouvés et que la clé USB a été identifiée

    for disk_name in ${find_disk}
    do
        if [ "${disk_name}" != ${KEY_DRIVE_FOUND} ] ; then
            HARD_DRIVE=$(echo ${disk_name} | cut -d / -f3)
        fi

    done

elif [ -n "${KEY_DRIVE}" ] ; then

    echo "${message_trouve_usb_avant} ${GREEN}${KEY_DRIVE}${NC}${message_trouve_usb_apres}"
    echo ${message_selection_usb}
    echo ""

    i=0
    for disk_name in ${find_disk}
    do
        if [ ! $(echo ${KEY_DRIVE} | grep ${disk_name}) ] ; then
            disk_model=$(sudo fdisk -l ${disk_name} | grep "Disk model" | cut -d : -f2)
            disk_size=$(sudo fdisk -l ${disk_name} | grep "Disk " | grep sector | cut -d : -f2 | cut -d B -f1)
            i=$(($i+1))
            echo "${YELLOW}${i}${NC} - ${GREEN}${disk_name} - model: ${disk_model} - size: ${disk_size}B${NC}"
        fi

    done

    echo -n "${YELLOW}${message_nombre}${NC}"

    echo -n "${ORANGE}"
    read read_disk_number
    echo -n "${NC}"

    # Test si c'est un nombre
    # Check if the entry is a number
    if echo ${read_disk_number} | grep -qE '^[0-9]+$'; then

        echo ""

    else

        echo -n "${RED}${message_mauvais_nombre_disque_dur}${NC}"
        echo "\a"

        read read_no_correct_number

        exit 0
    fi

    i=0
    macth_number=0

    for disk_name in ${find_disk}
    do
        if [ ! $(echo ${KEY_DRIVE} | grep ${disk_name}) ] ; then
            i=$(($i+1))
            if [ ${read_disk_number} -eq $i ] ; then
                HARD_DRIVE=${disk_name}
                macth_number=1
            fi
        fi
    done

    if [  ${macth_number} -eq 1 ] ; then

        HARD_DRIVE=$(echo ${HARD_DRIVE} | cut -d / -f3)

    else

        echo -n "${RED}${message_mauvais_selection_disque_dur}${NC}"
        echo "\a"

        read read_no_correct_number

        exit 0

    fi

fi

echo "${message_usb_disque_dur_avant} ${GREEN}${KEY_DRIVE}${NC} ${message_usb_disque_dur_apres}${GREEN}/dev/${HARD_DRIVE}${NC}"
echo ""

# Détérnimation s'il y a un préfixe à la partition du disque dur de référence
if [ $(echo ${HARD_DRIVE} | grep -e nvme -e mmcblk) ] ; then
    HARD_DRIVE_PREFIXE_PART="p"
else
    HARD_DRIVE_PREFIXE_PART=""
fi

# Changement nom du clone pour être compatible avec la clé dé réemploi
if test -d /sys/firmware/efi/ ; then

    if [ "$(mokutil --sb-state | grep "SecureBoot enabled")" != "" ] ; then
        NAME_CLONE=${NAME_CLONE}_UEFI_SB
    else
        NAME_CLONE=${NAME_CLONE}_UEFI
    fi

fi

# Contrôle taille disque de référence
# Reference disk size control


# Vérifier s'il n'y a pas trois partitions Linux, ce qui voudrait dire qu'il y a une partition séparée autre de / et /home
# Check if there are three Linux partitions, if you want to tell which one and a separate partition other than / and /home

HARD_DRIVE_SYSTEM_EXT=$(blkid | grep ${HARD_DRIVE}${HARD_DRIVE_PREFIXE_PART} | grep 'ext' | cut  -d ':' -f1)
HARD_DRIVE_SYSTEM_EXT=$(echo ${HARD_DRIVE_SYSTEM_EXT} | sed s/\\/dev\\///g)

HARD_DRIVE_SYSTEM_OTHER=$(blkid | grep ${HARD_DRIVE}${HARD_DRIVE_PREFIXE_PART} | grep 'btrfs\|xfs\|f2fs' | cut  -d ':' -f1)
HARD_DRIVE_SYSTEM_OTHER=$(echo ${HARD_DRIVE_SYSTEM_OTHER} | sed s/\\/dev\\///g)

NB_HARD_DRIVE_PARTITION_LINUX_EXT=$(echo ${HARD_DRIVE_SYSTEM_EXT} | wc -w)
NB_HARD_DRIVE_PARTITION_LINUX_OTHER=$(echo ${HARD_DRIVE_SYSTEM_OTHER} | wc -w)

# On affecte pour la partition système celle ayant un type btrfs\|xfs\|f2fs si elle existe,
# sinon le type ext
if [ ! -z ${HARD_DRIVE_SYSTEM_OTHER} ] ; then
    HARD_DRIVE_SYSTEM=${HARD_DRIVE_SYSTEM_OTHER}
else
    HARD_DRIVE_SYSTEM=${HARD_DRIVE_SYSTEM_EXT}
fi

if [ ! -z ${DEBUG} ] ; then
    echo "HARD_DRIVE_SYSTEM_EXT=${HARD_DRIVE_SYSTEM_EXT}"
    echo "HARD_DRIVE_SYSTEM_OTHER=${HARD_DRIVE_SYSTEM_OTHER}"
    echo "NB_HARD_DRIVE_PARTITION_LINUX_EXT=${NB_HARD_DRIVE_PARTITION_LINUX_EXT}"
    echo "NB_HARD_DRIVE_PARTITION_LINUX_OTHER=${NB_HARD_DRIVE_PARTITION_LINUX_OTHER}"
    echo ""
fi


if [ ${NB_HARD_DRIVE_PARTITION_LINUX_EXT} -gt 2 ] || [ ${NB_HARD_DRIVE_PARTITION_LINUX_OTHER} -gt 1 ] ; then

    echo "${RED}${message_nb_partition_linux}"
    echo "${message_nb_partition_linux_non_compatible}"
    echo "${message_schema_partition_url}${NC}"
    echo ""

    echo "${ORANGE}${message_schema_partition_ko1}${NC}"
    echo "${ORANGE}${message_schema_partition_ko2}${NC}"

    echo -n "${ORANGE}"
    read read_nb_max_hard_drive_partition_linux
    echo -n "${NC}"

    if [ "$read_nb_max_hard_drive_partition_linux" = ${CODE_EXIT_MINUSCULE} ] || [ "$read_nb_max_hard_drive_partition_linux" = ${CODE_EXIT_MAJUSCULE} ] ; then
        exit 4
    fi

fi

# Vérifier la taille de la partition / du disque dur de référence
# Check reference hard drive/partition size

point_mount_hdd="/home/mount_hdd"

if [ ! -d ${point_mount_hdd} ] ; then
    sudo mkdir ${point_mount_hdd}
fi

size_hard_drive_root_partition_used_total=0
for name_hard_drive_system in ${HARD_DRIVE_SYSTEM}
do

exist_disk=$(blkid | grep ${name_hard_drive_system} | grep 'ext\|btrfs\|xfs\|f2fs' | cut  -d ':' -f1 )

if [ ! -z ${exist_disk} ] ; then

    sudo mount /dev/${name_hard_drive_system} ${point_mount_hdd}

    # Lecture taille de la partition / du disque dur de référence
    # Read reference partition / hard disk size
    size_hard_drive_root_partition=$(df --output=size,source /dev/${name_hard_drive_system} | grep /dev/${name_hard_drive_system} | cut  -d '/' -f1 | sed  s/\ //g)

    # Lecture de la taille utile de la partition / du disque dur de référence
    # Reading the useful size of the reference partition / hard disk
    size_hard_drive_root_partition_used=$(df --output=used,source /dev/${name_hard_drive_system} | grep /dev/${name_hard_drive_system} | cut  -d '/' -f1 | sed  s/\ //g)
    size_hard_drive_root_partition_used_total=$((${size_hard_drive_root_partition_used_total}+${size_hard_drive_root_partition_used}))

    echo "HARD_DRIVE_SYSTEM=${name_hard_drive_system} : ${message_taille} = ${size_hard_drive_root_partition} K${symbole_octet}, ${message_taille_utilisee} = ${size_hard_drive_root_partition_used} K${symbole_octet}"

    sudo umount ${point_mount_hdd}

    # Contrôle de la taille préférence de la partition / du disque dur de référence

    if [ ! -z "${size_hard_drive_root_partition}" ] ; then
        if [ "${size_hard_drive_root_partition}" -gt "${MAX_SIZE_ROOT_PREFERED_KB}" ] ; then

            echo "${ORANGE}${message_taille_partition_superieure_avant} \"/\" (/dev/${name_hard_drive_system} = ${size_hard_drive_root_partition} K${symbole_octet}) ${message_taille_partition_superieure_apres} ${MAX_SIZE_ROOT_PREFERED}${symbole_octet},"
            echo "${message_taille_partition_root}${NC}"
            echo "${GREEN}${message_taille_partition_root_conseil}${NC}"
            echo ""

            echo "${ORANGE}${message_schema_partition_grande_ko1}${NC}"
            echo "${ORANGE}${message_schema_partition_grande_ko2}${NC}"

            echo -n "${ORANGE}"
            read read_max_prefered_size_root_hard_drive
            echo -n "${NC}"

            if [ "$read_max_prefered_size_root_hard_drive" = ${CODE_EXIT_MINUSCULE} ] || [ "$read_max_prefered_size_root_hard_drive" = ${CODE_EXIT_MAJUSCULE} ] ; then
                exit 5
            fi
        fi
    fi
fi

done

# Calcul de la taille de l'espace libre de la clé USB
# Calculating the free space size of the USB stick
size_key_drive_free=$(df --output=avail,source ${KEY_DRIVE} | grep ${KEY_DRIVE} | cut  -d '/' -f1 | sed  s/\ //g)

if [ ! -z ${DEBUG} ] ; then
    echo "size_hard_drive_root_partition_used_total=${size_hard_drive_root_partition_used_total} K${symbole_octet}"
    echo "size_key_drive_free=${size_key_drive_free} K${symbole_octet}"
    echo ""
fi

if [ ! -z "${size_hard_drive_root_partition_used_total}" ] && [ ! -z "${size_key_drive_free}" ] ; then

    if [ ${OPTION_FOR_COMPRESSION} = "-z0" ] ; then
        size_hard_drive_compressed=${size_hard_drive_root_partition_used_total}
    else
        # Estimation du facteur de division par 3
        size_hard_drive_compressed=$((${size_hard_drive_root_partition_used_total}/3))
    fi

    size_hard_drive_compressed=$((${size_hard_drive_root_partition_used_total}/3))

    if [ "${size_hard_drive_compressed}" -gt "${size_key_drive_free}" ] ; then

        echo "${RED}${message_taille_usb_trop_petite_debut} ${KEY_DRIVE} ${message_taille_usb_trop_petite_milieu} (${size_key_drive_free} K${symbole_octet})"
        echo "${message_taille_usb_trop_petite_fin} /dev/${HARD_DRIVE}${HARD_DRIVE_PREFIXE_PART}5 (${size_hard_drive_root_partition_used_total} K${symbole_octet})${NC}"
        echo ""

        echo "${ORANGE}${message_taille_usb_trop_petite_ko}${NC}"
        read read_size_key_drive_to_small

        exit 6
    fi

else

    echo ""
    echo "${ORANGE}${message_taille_partition_non_trouve1}${NC}"
    echo "${ORANGE}${message_taille_partition_non_trouve2}${NC}"
    echo ""

    echo -n "${ORANGE}"
    read read_size_key_drive_impossible
    echo -n "${NC}"

    if [ "$read_size_key_drive_impossible" = ${CODE_EXIT_MINUSCULE} ] || [ "$read_size_key_drive_impossible" = ${CODE_EXIT_MAJUSCULE} ] ; then
        exit 7
    fi

fi

if [ ! -z ${DEBUG} ] ; then
    echo "Pause"
    read pause
fi

# Réparation des partitions, car soucis avec la création de la partition home
echo ""
for disk_name in ${HARD_DRIVE_SYSTEM_EXT}
do
    echo "${ORANGE}${controle_partition}${disk_name}${NC}"
    sudo fsck /dev/${disk_name}
    echo ""
done

for disk_name in ${HARD_DRIVE_SYSTEM_OTHER}
do
    echo "${ORANGE}${controle_partition}${disk_name}${NC}"
    sudo fsck /dev/${disk_name}
    echo ""
done

echo "${ORANGE}sudo /usr/sbin/ocs-sr -q2 -c -j2 -i 4096 -batch ${CLONEZILLA_BEEP} ${OPTION_FOR_FINISH} ${OPTION_FOR_ENCRYPT} ${OPTION_FOR_CKECK_SAVE_IMAGE} ${OPTION_FOR_COMPRESSION} ${OPTION_FOR_CKECK_REPAIR} ${OPTIONS_FOR_GENERATE_CHECKSUM} savedisk ${DATE}-${NAME_CLONE} ${HARD_DRIVE}${NC}"
echo ""

sudo /usr/sbin/ocs-sr -q2 -c -j2 -i 4096 -batch ${CLONEZILLA_BEEP} ${OPTION_FOR_FINISH} ${OPTION_FOR_ENCRYPT} ${OPTION_FOR_CKECK_SAVE_IMAGE} ${OPTION_FOR_COMPRESSION} ${OPTION_FOR_CKECK_REPAIR} ${OPTIONS_FOR_GENERATE_CHECKSUM} savedisk ${DATE}-${NAME_CLONE} ${HARD_DRIVE}

